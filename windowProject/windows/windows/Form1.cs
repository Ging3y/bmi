﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace windows
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //Fact 1
        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("In Russia's Kronotsky Nature Reserve, there is a bear population that huffs jet fuel from old barrels until they get high and pass out. Did you like this fact?", "Fact 1", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
        }

        //Get Text
        private void txtBtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this.textBox1.Text, "Text Box");
           
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        //Fact Box 2
        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("There was an animal described as something between a horse and a gorilla, called a Chalicothere. Did you enjoy this fact?", "Fact 2", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
        }

        //Box 3
        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("When you remember a past event, you are actually remembering the last time you remembered it, not the event itself. Did this confuse you?", "Fact 3", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("If the human eye was a digital camera it would have 576 megapixels! Isn't that impressive?", "Fact 4", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
        }
    }
}
