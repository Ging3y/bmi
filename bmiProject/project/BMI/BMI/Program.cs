﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BMI
{
    class Program
    {
        static void Main(string[] args)
        {
            //Initialize our variables
            string name, weight, str_inches,str_feet;
            double feet, pounds, BMI, inches, inchHeight;

            //Set the title of the program
            Console.Title = "BMI Calculator";

            //Welcome the User
            Console.WriteLine("\tWelcome to the " + Console.Title + " program.\n");
            Console.WriteLine("I'm going to ask you your heigth, " +
                "weight, and name.\nI will then calculate your " +
                "Body Mass Index.\n");
            
            //Get info
            Console.Write("\nBefore we begin, what is your name?\n");
            name = Console.ReadLine();
            Console.WriteLine("\nIt's nice to met you, " + name + ".");

            //Get weight
            Console.Write("\nNow " + name +", I will ask you a few questions.");
            Console.Write("\n\nTo start off, what is your weight in pounds?\n");
            weight = Console.ReadLine();
            while (!Double.TryParse(weight, out pounds))
            {
                Console.Write("Please enter your weight as a number: \n");
                weight = Console.ReadLine();
            }


            //Get height
            Console.Write("\n\nWhat is your height?\n");
            Console.Write("Feet: ");
            str_feet = Console.ReadLine();
            while (!Double.TryParse(str_feet, out feet))
            {
                Console.Write("Please enter how many feet tall you are. Numbers only please! \n");
                str_feet = Console.ReadLine();
            }

            Console.Write("Inches: ");
            str_inches = Console.ReadLine();
            while (!Double.TryParse(str_inches, out inches))
            {
                Console.Write("Please enter how many inches tall you are. Numbers only please! \n");
                str_inches = Console.ReadLine();
            }



            //Print results
            Console.Write("\nThank you for your information.\n");

            inchHeight = (feet * 12) + inches;

            //Calculate BMI
            BMI = ((pounds * 703)/(inchHeight * inchHeight));
            //Round Answer to two Decimal Points
            BMI = Math.Truncate(BMI * 100) / 100;

            //Tell User
            Console.Write("Well the results are in " + name + "!\n");
            Console.Write("You have a BMI of " + BMI + "\n\n");
            Console.Write("Thank you for using this program!");
            Console.Write("\nPress Enter To Quit");
            Console.Read();


        }
    }
}
