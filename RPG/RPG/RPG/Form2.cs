﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RPG
{
    public partial class charForm : Form
    {
        //values of form
        string charName,race;
        string greeting;
        Boolean male;

        public charForm()
        {
            InitializeComponent();
        }

        //Gets all our form information
        public void getFormValues()
        {
          charName = charNameTextBox.Text;
          race = charComboBox.Text;

          if (maleRadioButton.Checked)
          {
              male = true;
              greeting = "Hello" + charName + ", male warrior, ";
          }

          if (femaleRadioButton.Checked)
          {
              male = false;
              greeting = "Hello" + charName + ", female warrior, ";
          }

          else
          {
              greeting = "Hello" + charName + ", of gender I haveth no clue, ";
          }

          
         

        }

        public void sendInfoMessage()
        {
            MessageBox.Show(greeting + "you are the most feared block in the geometry world." +
            "You are a " + race + ", ready to crush all foes around you.", "Creating your charecter");

        }
         

        private void exitButton_Click(object sender, EventArgs e)
        {
            //Make sure not to use .Close();
            //It crashes the program when you try to 
            //reopen the window
            this.Hide();
        }

        private void continueButton_Click(object sender, EventArgs e)
        {
            getFormValues();
            sendInfoMessage();
        }

       
    }
}
