﻿namespace RPG
{
	partial class charForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.exitButton = new System.Windows.Forms.Button();
            this.charNameLabel = new System.Windows.Forms.Label();
            this.charNameTextBox = new System.Windows.Forms.TextBox();
            this.maleRadioButton = new System.Windows.Forms.RadioButton();
            this.femaleRadioButton = new System.Windows.Forms.RadioButton();
            this.charComboBox = new System.Windows.Forms.ComboBox();
            this.charSelectLabel = new System.Windows.Forms.Label();
            this.continueButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(188, 227);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 23);
            this.exitButton.TabIndex = 1;
            this.exitButton.Text = "Cancel ";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // charNameLabel
            // 
            this.charNameLabel.AutoSize = true;
            this.charNameLabel.Location = new System.Drawing.Point(13, 13);
            this.charNameLabel.Name = "charNameLabel";
            this.charNameLabel.Size = new System.Drawing.Size(38, 13);
            this.charNameLabel.TabIndex = 2;
            this.charNameLabel.Text = "Name:";
            // 
            // charNameTextBox
            // 
            this.charNameTextBox.Location = new System.Drawing.Point(58, 13);
            this.charNameTextBox.Name = "charNameTextBox";
            this.charNameTextBox.Size = new System.Drawing.Size(128, 20);
            this.charNameTextBox.TabIndex = 3;
            // 
            // maleRadioButton
            // 
            this.maleRadioButton.AutoSize = true;
            this.maleRadioButton.Location = new System.Drawing.Point(204, 16);
            this.maleRadioButton.Name = "maleRadioButton";
            this.maleRadioButton.Size = new System.Drawing.Size(48, 17);
            this.maleRadioButton.TabIndex = 4;
            this.maleRadioButton.TabStop = true;
            this.maleRadioButton.Text = "Male";
            this.maleRadioButton.UseVisualStyleBackColor = true;
            // 
            // femaleRadioButton
            // 
            this.femaleRadioButton.AutoSize = true;
            this.femaleRadioButton.Location = new System.Drawing.Point(204, 60);
            this.femaleRadioButton.Name = "femaleRadioButton";
            this.femaleRadioButton.Size = new System.Drawing.Size(59, 17);
            this.femaleRadioButton.TabIndex = 5;
            this.femaleRadioButton.TabStop = true;
            this.femaleRadioButton.Text = "Female";
            this.femaleRadioButton.UseVisualStyleBackColor = true;
            // 
            // charComboBox
            // 
            this.charComboBox.FormattingEnabled = true;
            this.charComboBox.Items.AddRange(new object[] {
            "Fast Block",
            "Heavy Block",
            "Smart Block",
            "Lazy Block",
            "Bob The Chicken"});
            this.charComboBox.Location = new System.Drawing.Point(57, 60);
            this.charComboBox.Name = "charComboBox";
            this.charComboBox.Size = new System.Drawing.Size(129, 21);
            this.charComboBox.TabIndex = 6;
            this.charComboBox.Text = "Select your charecter";
            // 
            // charSelectLabel
            // 
            this.charSelectLabel.AutoSize = true;
            this.charSelectLabel.Location = new System.Drawing.Point(16, 62);
            this.charSelectLabel.Name = "charSelectLabel";
            this.charSelectLabel.Size = new System.Drawing.Size(35, 13);
            this.charSelectLabel.TabIndex = 7;
            this.charSelectLabel.Text = "Class:";
            // 
            // continueButton
            // 
            this.continueButton.Location = new System.Drawing.Point(19, 227);
            this.continueButton.Name = "continueButton";
            this.continueButton.Size = new System.Drawing.Size(75, 23);
            this.continueButton.TabIndex = 8;
            this.continueButton.Text = "Continue";
            this.continueButton.UseVisualStyleBackColor = true;
            this.continueButton.Click += new System.EventHandler(this.continueButton_Click);
            // 
            // charForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.continueButton);
            this.Controls.Add(this.charSelectLabel);
            this.Controls.Add(this.charComboBox);
            this.Controls.Add(this.femaleRadioButton);
            this.Controls.Add(this.maleRadioButton);
            this.Controls.Add(this.charNameTextBox);
            this.Controls.Add(this.charNameLabel);
            this.Controls.Add(this.exitButton);
            this.MaximumSize = new System.Drawing.Size(300, 300);
            this.MinimumSize = new System.Drawing.Size(300, 300);
            this.Name = "charForm";
            this.Text = "Charecter Creation";
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Label charNameLabel;
        private System.Windows.Forms.TextBox charNameTextBox;
        private System.Windows.Forms.RadioButton maleRadioButton;
        private System.Windows.Forms.RadioButton femaleRadioButton;
        private System.Windows.Forms.ComboBox charComboBox;
        private System.Windows.Forms.Label charSelectLabel;
        private System.Windows.Forms.Button continueButton;

    }
}