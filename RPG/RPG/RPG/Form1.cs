﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RPG
{
    public partial class Blocks : Form
    {
        public Blocks()
        {
            InitializeComponent();
        }

        //When they press quit, end the program
        private void quitButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you would like to quit?",
                "Blocks", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        //Create new Charecter window
        charForm secondForm = new charForm();
        private void createcharButton_Click(object sender, EventArgs e)
        {
            secondForm.Show();
            
            
        }

  

       
    }
}
